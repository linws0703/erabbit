import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  // 原始代码
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]
// vue2.0 new VueRouter({routes})
// vue3.0 createRouter({routes})
const router = createRouter({
  history: createWebHashHistory(), // 使用hash模式，即 带 # 的地址格式
  routes
})

export default router
