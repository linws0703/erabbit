import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// router和store是插件，那么就用use
createApp(App).use(store).use(router).mount('#app')
