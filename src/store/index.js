import { createStore } from 'vuex'

// A模块
const moduleA = {
  state: {},
  getters: {},
  mutations: {},
  actions: {}
}
// B模块
const moduleB = {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {}
}

export default createStore({
  modules: {
    moduleA,
    moduleB
  }
})

// vue2.0 创建仓库 new Vuex.Store({})
// vue3.0 创建仓库 createStore({})
// export default createStore({
//   state: {
//     username: 'zs'
//   },
//   getters: {
//     newName(state) {
//       return state.username + '2333'
//     }
//   },
//   mutations: {
//     updateName(state, value) {
//       state.username = value
//     }
//   },
//   actions: {
//     updateName(context, value) {
//       // 发请求，异步操作
//       setTimeout(() => {
//         context.commit('updateName', value)
//       }, 1000)
//     }
//   },
//   modules: {
//   }
// })
